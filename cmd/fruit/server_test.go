package main

import (
	"encoding/json"
	"gitlab.com/valeness/fruit/internal/util"
	fruit "gitlab.com/valeness/fruit/pkg/model"
	"io/ioutil"
	"net/http"
	"net/http/httptest"

	"testing"
)

func TestRouter(t *testing.T) {
	w := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	handler := http.HandlerFunc(indexHandler)
	handler.ServeHTTP(w, req)

	expected := "Tea. Earl Grey. Hot."
	actual := w.Body.String()
	if expected != actual {
		t.Fatalf("Expected %s but got %s", expected, actual)
	}
}

func TestFruits(t *testing.T) {
	_ = util.SeedFruits("../../data/fruits.json")
	w := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodGet, "/fruit", nil)

	handler := http.HandlerFunc(fruitHandler)
	handler.ServeHTTP(w, req)

	fruits := []fruit.Fruit{}
	t.Log(w.Body.String())
	bytes, _ := ioutil.ReadAll(w.Body)
	err := json.Unmarshal(bytes, &fruits)
	if err != nil {
		t.Fatalf("Could not parse /fruit as json")
	}
}

func TestFilterFruitsName(t *testing.T) {
	_ = util.SeedFruits("../../data/fruits.json")
	w := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodGet, "/fruit?name=app", nil)

	handler := http.HandlerFunc(fruitHandler)
	handler.ServeHTTP(w, req)

	fruits := []fruit.Fruit{}
	t.Log(w.Body.String())
	bytes, _ := ioutil.ReadAll(w.Body)
	err := json.Unmarshal(bytes, &fruits)
	if err != nil {
		t.Fatalf("Could not parse /fruit as json")
	}

	for _, fruitItem := range fruits {
		if fruitItem.Name[0:3] != "App" {
			t.Fatalf("Could not successfully filter on Name")
		}
	}

}

func TestFilterFruitsInSeason(t *testing.T) {
	_ = util.SeedFruits("../../data/fruits.json")
	w := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodGet, "/fruit?inSeason=true", nil)

	handler := http.HandlerFunc(fruitHandler)
	handler.ServeHTTP(w, req)

	fruits := []fruit.Fruit{}
	t.Log(w.Body.String())
	bytes, _ := ioutil.ReadAll(w.Body)
	err := json.Unmarshal(bytes, &fruits)
	if err != nil {
		t.Fatalf("Could not parse /fruit as json")
	}

	for _, fruitItem := range fruits {
		t.Log(fruitItem.InSeason)
		if !fruitItem.InSeason {
			t.Fatalf("Could not successfully filter on inSeason")
		}
	}

}

func TestFruitsNotFound(t *testing.T) {
	_ = util.SeedFruits("../../data/fruits.json")
	w := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodGet, "/fruit?name=defnotafruitname", nil)

	handler := http.HandlerFunc(fruitHandler)
	handler.ServeHTTP(w, req)

	apiResponse := ApiResponse{}
	t.Log(w.Body.String())
	bytes, _ := ioutil.ReadAll(w.Body)
	err := json.Unmarshal(bytes, &apiResponse)
	if err != nil {
		t.Fatalf("Could not parse 204 response as json")
	}

	if apiResponse.Code != 204 {
		t.Fatalf("Response code is not 404 for empty fruits")
	}

}