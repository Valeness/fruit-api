package main

import (
	"encoding/json"
	"github.com/gorilla/schema"
	"gitlab.com/valeness/fruit/internal/util"
	"gitlab.com/valeness/fruit/pkg/model"
	"log"
	"net/http"
	"os"
	"reflect"
)

var port = "8080"

type Filter struct {
	Name   string `schema:"name"`
	InSeason   string `schema:"inSeason"`
	Colors   string `schema:"color"`
}

type Pagination struct {
	Offset   int `schema:"offset"`
	Limit   int `schema:"limit"`
}

type ApiResponse struct {
	Message string
	Code int
}

func throwError(w http.ResponseWriter, message string, code int) {
	res := ApiResponse{
		Message: message,
		Code:    code,
	}
	err := json.NewEncoder(w).Encode(res)

	if err != nil {
		log.Println("Error writing error to ResponseWriter - Falling back to log error handler")
		log.Println(err)
		log.Println("[ERROR] " + message)

	}
}

func main() {
	wd, err := os.Getwd()

	if err != nil {
		log.Println("Could not get working directory")
		wd = "."
	}

	err = util.SeedFruits(wd + "/data/fruits.json")

	if err != nil {
		log.Println(err)
	} else {
		log.Println("Seeded Fruits into Memory")
	}

	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/fruit", fruitHandler)

	log.Fatal(http.ListenAndServe(":"+os.Getenv("PORT"), nil))
}

func fruitHandler(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	fruits := []fruit.Fruit{}

	err := req.ParseForm()

	if err != nil {
		throwError(w, err.Error(), 405)
		return
	}

	filters := new(Filter)
	d := schema.NewDecoder()
	d.IgnoreUnknownKeys(true)
	if err := d.Decode(filters, req.Form); err != nil {
		log.Println(err)
		throwError(w, "Could not parse filters", 405)
		return
	}

	pagination := new(Pagination)
	d = schema.NewDecoder()
	d.IgnoreUnknownKeys(true)
	if err := d.Decode(pagination, req.Form); err != nil {
		throwError(w, "Could not parse pagination", 405)
		return
	}

	allFilters := []fruit.Filter{}

	v := reflect.ValueOf(*filters)

	values := make([]interface{}, v.NumField())

	for i := 0; i < v.NumField(); i++ {
		values[i] = v.Field(i).Interface()
		if v.Field(i).String() != "" {
			allFilters = append(allFilters, fruit.Filter{
				Key:   v.Type().Field(i).Name,
				Value: v.Field(i).String(),
			})
		}
	}

	offset := pagination.Offset
	limit := pagination.Limit


	for _, fruitItem := range fruit.Fruits {
		pass := fruit.FilterFruit(fruitItem, allFilters)

		if pass {
			fruits = append(fruits, fruitItem)
		}
	}

	fruits = fruit.GetPaginatedSlice(offset, limit, fruits)


	if len(fruits) <= 0 {
		throwError(w, "No fruits found", 204)
		return
	}

	err = json.NewEncoder(w).Encode(fruits)

	if err != nil {
		throwError(w, "Internal Server Error", 500)
	}

}

func indexHandler(w http.ResponseWriter, req *http.Request) {

	_, err := w.Write([]byte("Tea. Earl Grey. Hot."))


	if err != nil {
		throwError(w, "Internal Server Error", 405)
	}

}