FROM golang:1.10-alpine3.7 as build

WORKDIR /go/src/gitlab.com/valeness/fruit/

COPY . .

RUN go build -o app cmd/fruit/server.go

FROM alpine:3.7

COPY --from=build /go/src/gitlab.com/valeness/fruit/app /usr/local/bin/app
COPY --from=build /go/src/gitlab.com/valeness/fruit/data /data

CMD "/usr/local/bin/app"