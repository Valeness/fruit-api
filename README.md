# Fruits API
This is a super advanced API that uses Machine Learning to determine exactly which fruit will sate your hunger before you even know you're hungry!

Test the Fruits API at : https://fruits-api-test.herokuapp.com/fruit