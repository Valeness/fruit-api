package fruit

import (
	"log"
	"reflect"
	"strconv"
	"strings"
)

var Fruits []Fruit
var DEFAULTLIMIT = 5

type Fruit struct {
	Name string `json:"name"`
	Colors []string `json:"colors"`
	InSeason bool `json:"inSeason"`
}

type Filter struct {
	Key string
	Value string
}

type FieldInfo struct {
	IsValid bool
	Value reflect.Value
	FieldType reflect.Kind
}

func sliceContains(val string, slice []string) bool {
	for _, v := range slice {
		if val == v {
			return true
		}
	}

	return false
}

func (fruit Fruit) GetFieldInfo(field string) FieldInfo {
	r := reflect.ValueOf(fruit)
	fieldValue := r.FieldByName(strings.Title(field))


	fieldInfo := FieldInfo{
		IsValid:   fieldValue.IsValid(),
		Value: reflect.Value{},
	}

	if fieldInfo.IsValid {
		fieldInfo.Value = fieldValue
		fieldInfo.FieldType = fieldValue.Kind()
	}

	return fieldInfo
}

func GetPaginatedSlice(offset int, limit int, fruits []Fruit) []Fruit {
	totalFruitCount := len(fruits)


	if limit <= 0 {
		limit = DEFAULTLIMIT
	}

	sliceLimit := offset + limit

	if sliceLimit > totalFruitCount  {
		sliceLimit = totalFruitCount
	}

	if offset > totalFruitCount {
		offset = totalFruitCount
	} else if offset <= 0 {
		offset = 0
	}

	return fruits[offset:sliceLimit]

}

func AddFruit(fruit Fruit) {
	Fruits = append(Fruits, fruit)
}

func FilterFruit(fruit Fruit, filters []Filter) bool {
	pass := true
	for _, f := range filters {

		key := f.Key
		value := strings.ToLower(f.Value)
		info := fruit.GetFieldInfo(key)

		switch info.FieldType {
		case reflect.String:

			filterValueLength := len(value)
			fruitValueLength := len(info.Value.String())

			if fruitValueLength < filterValueLength {
				pass = false
				break
			}

			fruitSlice := strings.ToLower(info.Value.String()[0:filterValueLength])

			pass = value == fruitSlice

			break

		case reflect.Bool:
			boolFilterValue, _ := strconv.ParseBool(value)
			boolFruitValue := info.Value.Bool()

			pass = boolFilterValue == boolFruitValue

			break

		case reflect.Slice:

			log.Println(info.Value)

			colors := info.Value.Interface().([]string)
			pass = sliceContains(value, colors)

			break

		default:
			pass = false
			break
		}

		if pass {
			break
		}

	}

	return pass
}