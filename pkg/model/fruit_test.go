package fruit

import (
	"testing"
)

func TestAddFruit(t *testing.T) {

	AddFruit(Fruit{
		Name:     "Apple",
		Colors:   []string{"red", "yellow"},
		InSeason: false,
	})

	if len(Fruits) != 1 {
		t.Fatal("Could not add fruit to in memory Fruits slice")
	}

}

func TestFilterFruitName(t *testing.T) {
	apple := Fruit{
		Name : "Apple",
		Colors : []string{"red", "yellow"},
		InSeason : true,
	}

	passFilterName := FilterFruit(apple, []Filter{
		{
			Key : "name",
			Value : "App",
		},
	})

	if !passFilterName {
		t.Fatal("Could not match 'App' to 'Apple' during fruit name filtering")
	}

}

func TestFilterFruitInSeason(t *testing.T) {
	banana := Fruit{
		Name : "Banana",
		Colors : []string{"yellow"},
		InSeason : true,
	}

	passFilterInSeason := FilterFruit(banana, []Filter{
		{
			Key : "inSeason",
			Value : "true",
		},
	})

	t.Log(passFilterInSeason)

	if !passFilterInSeason {
		t.Fatal("Could not match 'true' to 'true' during fruit inSeason filtering")
	}

}
func TestFilterFruitColors(t *testing.T) {
	apple := Fruit{
		Name : "Apple",
		Colors : []string{"yellow", "red", "green"},
		InSeason : true,
	}

	passFilterName := FilterFruit(apple, []Filter{
		{
			Key : "colors",
			Value : "green",
		},
	})

	if !passFilterName {
		t.Fatal("Could not match 'green' to '[yellow, red, green]' during fruit colors filtering")
	}

}