package util

import (
	"encoding/json"
	"errors"
	"gitlab.com/valeness/fruit/pkg/model"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

func SeedFruits(filePath string) error {

	seedError := errors.New("Failed to seed fruits data")

	absFilepath, err := filepath.Abs(filePath)

	if err != nil {
		return errors.New("Could not get absolute filepath")
	}

	f, err := os.Open(absFilepath)

	if err != nil {
		log.Println(err)
		return seedError
	}

	defer f.Close()

	bytes, err := ioutil.ReadAll(f)

	if err != nil {
		log.Println(err)
		return seedError
	}

	err = json.Unmarshal(bytes, &fruit.Fruits)

	if err != nil {
		log.Println(err)
		return seedError
	}

	return nil

}
