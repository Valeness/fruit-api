package util

import (
	fruit "gitlab.com/valeness/fruit/pkg/model"
	"testing"
)

func TestSeedFruits(t *testing.T) {
	err := SeedFruits("../../data/fruits.json")

	if err != nil {
		t.Fatal("Could not seed fruits into memory")
	}

	if len(fruit.Fruits) == 0 {
		t.Fatal("Could not seed fruits into memory")
	}

}

func TestSeedFruitsWrongFile(t *testing.T) {
	err := SeedFruits("/does/not/exist/fruits.json")

	if err == nil {
		t.Fatal("Expected error when loading nonexistent file into SeedFruits")
	}

}